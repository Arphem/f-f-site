<footer id="footer" class="Footer">
        <div class="foot_blocks">
        <div class="foot_contacts">
            <div class="text_contacts">
                <span>Contacts</span><br>
                <img id="foot_envelope" src="<?php bloginfo('template_url'); ?>./assets/img/icons/envelope_icon.png">
                <span id="foot-email"><?php the_field('footer-email')?></span>
                <div class="phone_numbers">
                <img id="foot_phone" src="<?php bloginfo('template_url'); ?>./assets/img/icons/phone_icon.png">
                <span><?php the_field('footer-phonefirst')?></span><br>
                <span><?php the_field('footer-phonesecond')?></span>
                </div>
            </div>
        </div>
        <div class="foot_address">
            <span id="foot-address-title">Address</span>
            <span id="foot_content-address"> <img id="map_icon" src="<?php bloginfo('template_url'); ?>./assets/img/icons/map_icon.png"><?php the_field('footer-address')?></span>
        </div>
        <div class="foot_socialmedia">
            <span id="sm_firstline">We are on</span>
            <span id="sm_secondline">Social Media</span>
            <div class="sm_icons">
                <a title="Twitter" href=""><img src="<?php bloginfo('template_url'); ?>./assets/img/icons/twitter.png" alt="twitter"></a>
                <a title="Instagram" href=""><img src="<?php bloginfo('template_url'); ?>./assets/img/icons/instagram.png" alt="instagram"></a>
                <a title="Facebook" href=""><img src="<?php bloginfo('template_url'); ?>./assets/img/icons/facebook.png" alt="facebook"></a>
                <a title="Behance" href=""><img src="<?php bloginfo('template_url'); ?>./assets/img/icons/behance.png" alt="behance"></a>
            </div>
        </div>
    </div>
    </footer>

    <?php wp_footer(); ?>
    <!-- <script src="./script.js"></script> -->
</body>
</html>