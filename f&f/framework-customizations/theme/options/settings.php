<?php if (!defined('FW')) die('Silence is golden.');

$options = array(
    'logo-image' => array(
    'type' => 'upload',
    'label' => __('Logo', 'f&f'),
    'images_only' => true
)
);