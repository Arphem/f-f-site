<?php

// правильный способ подключить стили и скрипты
add_action( 'wp_enqueue_scripts', 'f_f_style' );
// add_action('wp_print_styles', 'theme_name_scripts'); // можно использовать этот хук он более поздний
function f_f_style() {
	wp_enqueue_style( 'main-style', get_stylesheet_uri() );
    // wp_enqueue_script( 'script.js', get_template_directory_uri() . '/js/example.js', array(), '1.0.0', true );
    
}


// правильный способ подключить стили и скрипты
add_action( 'wp_enqueue_scripts', 'f_f_scripts' );

function f_f_scripts() {
    wp_enqueue_script( 'main-script', get_template_directory_uri() . '/assets/js/script.js', array(), '1.0.0', true );
}
