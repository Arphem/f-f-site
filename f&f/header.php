<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php bloginfo('description'); ?></title>
    <!-- <link rel="stylesheet" href="./style.css">
    <link rel="shortcut icon" href="./images/favicon.ico" /> -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->

    <?php wp_head(); ?>
</head>
<body>
    
    <header id="menu" class="menu">       
            <div class="logo">
                <img src="<?php bloginfo('template_url'); ?>./assets/img/Logo.png">
            </div>
                <nav>
                    <div id="myTopnav" class="topnav">
                        <a href="javascript:void(0);" class="icon" onclick="ResponsiveMenu()">
                            <div id="nav-icon3">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                          </div>
                        </a>
                        <a class="cool-link" href="#favours">Favours</a>
                        <a class="cool-link" href="#aboutUs">About us</a>
                        <a class="cool-link" href="#howCanYouHelp">How can you help?</a>
                        <a class="cool-link" href="#title_regform-block">Join us</a>
                        <a class="cool-link" href="#foot">Contacts</a>
                        
                    </div>  
                </nav>         
    </header>