<?php get_header(); ?>
<main>
    <section id="openpage" class="OpenPage">
            <div class="vignette"></div>
            <article class="header_text">
                <h4><?php the_field('header_text_first_phrase')?></h4>
                <h1><?php the_field('header_text_main_phrase')?></h1>
            </article>  
    </section>
    <section id="favours" class="Favours">
            <h1 id="fav_title"><?php the_field('fav_title')?></h1>
            <div id="fav_title_underline"></div>
            <article class="description_second_block"><?php the_field('description_first_line')?></br><?php the_field('description_second_line')?></article>
                <div class="wrapper">
                    <div class="card ">
                        <div class="image-overflow">
                            <div id="img1" class="image image1" >
                                <div  class="dandruff">
                                    <img src="<?php bloginfo('template_url'); ?>./assets/img/Dandruff.png">
                                </div>
                        </div>
                    </div>
                        <span class="ln_name">Self Aware</span>
                    </div>
                    <div class="card">
                        <div class="image-overflow">
                            <div id="img2" class="image image2">
                                <div  class="dandruff">
                                    <img src="<?php bloginfo('template_url'); ?>./assets/img/Dandruff.png">
                                </div>
                        </div>
                    </div>
                        <h3 class="ln_name">World Citizen</h3>
                    </div>
                    <div class="card">
                        <div class="image-overflow">
                            <div id="img3" class="image image3">
                                <div  class="dandruff">
                                    <img src="<?php bloginfo('template_url'); ?>./assets/img/Dandruff.png">
                                </div>
                        </div>
                    </div>
                        <h3 class="ln_name">Solution Oriented</h3>
                    </div>
                    <div class="card">
                        <div class="image-overflow">
                            <div id="img4" class="image image4">
                                <div  class="dandruff">
                                    <img src="<?php bloginfo('template_url'); ?>./assets/img/Dandruff.png">
                                </div>
                        </div>
                    </div>
                        <h3 class="ln_name">Ability to Empower<br> Others</h3>
                    </div>
                </div>
    </section>
       
    <section id="aboutus-howcanyouhelp" class="AboutUs-HowCanYouHelp">
        <div id="aboutUs" class="AboutUs">
            <div class="vignette-aboutUs"></div>
            <h1 id="AboutUs-title"><?php the_field('aboutus-title')?></h1>
                <div id="About-Title-Underline"></div>
                <div class="AboutUs-content">
                    <div class="pattern_left-aboutus">
                        <img id="Left-Pattern-aboutus" src="<?php bloginfo('template_url'); ?>./assets/img/pattern_left-aboutUs.png">
                        </div>
                    <article class="aboutUs_text">
                        <p id="first_line"><?php the_field('aboutus_first_line')?></p>
                        <p id="second_line"><?php the_field('aboutus_second_line')?></p>
                        <p id="third_line"><?php the_field('aboutus_third_line')?></p>
                    </article>
                    <div class="pattern_right-aboutus">
                        <img id="Right-Pattern-aboutus" src="<?php bloginfo('template_url'); ?>./assets/img/pattern_right-aboutUs.png">
                    </div>
                </div>
        </div>

        <div id="howCanYouHelp" class="howCanYouHelp">
            <div class="between_line"></div>
            <h1 id="HCYH-title"><?php the_field('hcyh-title')?></h1>
            <div id="HCYH-title-underline"></div>
            <div class="howCanYouHelp-content">
                <div class="pattern_left-HCYH">
                    <img id="HCYH-left-patimage" src="<?php bloginfo('template_url'); ?>./assets/img/pattern_left-HCYH.png">
                </div>   
                <article class="HCYH-text">
                            <div class="adv-paragraph">
                                <div id="adv_title_first">
                                    <span id="first_adv_title"><?php the_field('first_adv_title')?></span>
                                </div>
                                <p><?php the_field('first_adv_line')?></p>   
                                    <div id="adv_title_second">
                                        <span id="second_adv_title"><?php the_field('second_adv_title')?></span>
                                    </div>           
                                <p id="adv_text_second">
                                <?php the_field('second_adv_firstline')?></p>
                                </div>
                                <div class="HCYH_ul_wrap">
                                    <div id="adv_title_third">
                                        <span id="third_title_adv"><?php the_field('third_title_adv')?></span>
                                    </div>
                                    <ul class="adv_list">
                                        <li><?php the_field('third_adv_firstli')?></li>
                                        <li><?php the_field('third_adv_secondli')?></li>
                                        <li><?php the_field('third_adv_thirdli')?></li>
                                        <li><?php the_field('third_adv_fourthli')?></li>
                                        <li><?php the_field('third_adv_fifthli')?></li>
                                    </ul>
                                </div>  
                </article>    
                <div class="pattern_right-HCYH">
                    <img id="HCYH-left-patimage" src="<?php bloginfo('template_url'); ?>./assets/img/pattern_right-HCYH.png">
                </div>        
            </div>            
        </div>
    </section>

    <section class="Regform-block">
            <h1 id="title_regform-block">What do you need to join us?</h1>
            <div id="underline_title-regform"></div>
            <div class="Regform-content">

                <div class="pattern_left-regform">
                    <img class="left_pattern" src="<?php bloginfo('template_url'); ?>./assets/img/pattern_left-regform.png">
                </div>

                    <div class="Reg-Form">
                        <form id="reg-form">
                            <div class="regform-blocks">
                                <div class="first_three-reg-inputs">
                                    <div class=""><label for="block1">Your e-mail</label><br>
                                        <input type="email" pattern="^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$" id="block1" class="regblock" required><br></div>
                                    <div class="ri"><label for="block2">Your name</label><br>
                                        <input id="block2" class="regblock" required><br></div>
                                    <div class="ri">
                                        <label for="block3">Your Surname</label><br>
                                    <input id="block3" class="regblock" required><br>
                                    </div>      
                                </div>
                                <div class="second_three-reg-inputs">
                                    <div><label for="block4">Your Phone Number</label><br>
                                        <input id="block4" class="regblock" required><br>
                                    </div>
                                    <div class="ri">
                                        <label for="block5">Your Country</label><br>
                                        <input id="block5" class="regblock" required><br>
                                    </div>
                                    <div class="ri">
                                        <label for="block6">Your city</label><br>
                                        <input id="block6" class="regblock" required><br>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <button id="btn-regform" type="submit" form="reg-form">Join</button>
                    </div>

                <div class="pattern_right-regform">
                        <img class="right_pattern" src="<?php bloginfo('template_url'); ?>./assets/img/pattern_right-regform.png">
                </div>

            </div>
    </section>
    </main>
    <?php get_footer(); ?>
